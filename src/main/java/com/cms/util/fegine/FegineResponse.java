package com.cms.util.fegine;

public class FegineResponse {

	private String status;
	private String msg;
	private FegineResult result;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public FegineResult getResult() {
		return result;
	}
	public void setResult(FegineResult result) {
		this.result = result;
	}
}
