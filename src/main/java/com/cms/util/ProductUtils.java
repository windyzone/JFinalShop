package com.cms.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.cms.CommonAttribute;
import com.jfinal.kit.PathKit;

public class ProductUtils {
	
	/** 通过指定参数写一个图片  */
    public static boolean writeImage(BufferedImage bi, String picType, File file,int width,int height) {
   
    	Graphics g = bi.createGraphics();
//设置底片的颜色
    	Color color = new Color(255,255,255);
        g.setColor(color);
//底片的宽高为多少.如果不写宽高你的图片创建出来是黑色的
        g.fillRect(0, 0, width, height);
        g.dispose();
        bi.flush();
        boolean val = false;
        try {
            val = ImageIO.write(bi, picType, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return val;
    }
    /**
     * 获取图片宽度
     * @param file  图片文件
     * @return 宽度
     */
    public static int getImgWidth(File file) {
        InputStream is = null;
        BufferedImage src = null;
        int ret = -1;
        try {
            is = new FileInputStream(file);
            src = javax.imageio.ImageIO.read(is);
            ret = src.getWidth(null); // 得到源图宽
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }


	public static void createImage(String mainPath){
		BufferedImage bi = new BufferedImage(300, 500, BufferedImage.TYPE_INT_BGR);
		//创建出来的图片存储的路径
		final File file = new File(mainPath);
		
		if(file.exists()) {
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		writeImage(bi, "jpg", file,300,500);
	}
	
	
	public static String qrImage(String avatar,String nickname,String productImg,String productName,String productPrice,String url){
		try{
			//下载头像
			String avatarFilePath = PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/";
			String avatarFileName = UUID.randomUUID().toString()+".jpg";
			HttpClientUtil.downloadFile(avatar, avatarFilePath+avatarFileName);
			String avatarPath = avatarFilePath + avatarFileName;
			//下载产品图片
			String productImgFilePath = PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/";
			String productImgFileName = UUID.randomUUID().toString()+".jpg";
			HttpClientUtil.downloadFile(productImg, productImgFilePath+productImgFileName);
			String productImgPath = productImgFilePath + productImgFileName;
			//生成产品二维码
    		String qr = QrcodeUtils.build(url, 400, 400);
    		String qrPath = PathKit.getWebRootPath()+qr;
			//主图
			String zhutuPath = PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/zhutu.jpg";
			String mainPath = PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/"+UUID.randomUUID().toString()+".jpg";;
			FileUtils.copyFile(new File(zhutuPath), new File(mainPath));
			//最终生成的
			String newQrImageName = UUID.randomUUID().toString()+".jpg";
			String newQrImagePath = PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/"+newQrImageName;
			//创建主图
			//createImage(mainPath);
			//1.jpg是你的 主图片的路径
	        InputStream is = new FileInputStream(mainPath);
	        //BufferedImage
	        BufferedImage buffImg = ImageIO.read(is);
	        //得到画笔对象
	        Graphics g = buffImg.getGraphics();
	        //头像
	        ImageIcon avatarIcon = new ImageIcon(avatarPath); 
	        Image avatarImage = avatarIcon.getImage();
	        g.drawImage(avatarImage,342,12,149,149,null);
	        
	        //设置颜色。
	        g.setColor(Color.BLACK);
	        //最后一个参数用来设置字体的大小
	        Font nameFont = new Font("微软雅黑",Font.BOLD,30);
	        g.setFont(nameFont);
	        //10,20 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
	        g.drawString(nickname+"分享给你一个商品",500,100);
	        
	        //创建你要附加的图象。
	        //2.jpg是你的小图片的路径
	        ImageIcon productIcon = new ImageIcon(productImgPath); 
	        //得到Image对象。
	        Image productImage = productIcon.getImage();
	        //将小图片绘到大图片上。
	        //5,300 .表示你的小图片在大图片上的位置。
	        g.drawImage(productImage,352,163,700,700,null);

	        //2.jpg是你的小图片的路径
	        ImageIcon qrIcon = new ImageIcon(qrPath); 
	        //得到Image对象。
	        Image qrImage = qrIcon.getImage();
	        //将小图片绘到大图片上。
	        //5,300 .表示你的小图片在大图片上的位置。
	        g.drawImage(qrImage,645,1087,280,280,null);
	        
	        //设置颜色。
	        g.setColor(Color.RED);
	        //最后一个参数用来设置字体的大小
	        Font priceFont = new Font("微软雅黑",Font.BOLD,70);
	        g.setFont(priceFont);
	        //10,20 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
	        g.drawString("￥ "+productPrice,10,990);
	        
	        if(productName.length()>20){
	        	productName = productName.substring(0,20)+"...";
	        }
	        String firstName = "";
	        String secondName = "";
	        if(productName.length()>10){
	        	firstName = productName.substring(0,10);
	        	if(productName.length()>17){
	        		secondName = productName.substring(10,17)+"...";
	        	}else{
	        		secondName = productName.substring(10,productName.length());
	        	}
	        }else{
	        	firstName = productName;
	        }
	        //设置颜色。
	        g.setColor(Color.BLACK);
	        //最后一个参数用来设置字体的大小
	        Font f = new Font("微软雅黑",Font.BOLD,50);
	        g.setFont(f);
	        //10,20 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
	        g.drawString(firstName,35,1150);
	        
	        if(StringUtils.isNotBlank(secondName)){
	        	 //最后一个参数用来设置字体的大小
		        Font secondNameFont = new Font("微软雅黑",Font.BOLD,50);
		        g.setFont(secondNameFont);
		        //10,20 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
		        g.drawString(secondName,35,1210);
	        }
	        
	        g.dispose();
	        OutputStream os = new FileOutputStream(newQrImagePath);
	        //创键编码器，用于编码内存中的图象数据。
	        ImageIO.write(buffImg, "jpg", os);
	        os.flush();
	        is.close();
	        os.close();
	        OSSUtils.upload(newQrImageName,new File(newQrImagePath), "image/jpeg");
	        System.out.println ("合成结束。。。。。。。。");
	        //清除
	        FileUtils.deleteQuietly(new File(avatarPath));
	        FileUtils.deleteQuietly(new File(productImgPath));
	        FileUtils.deleteQuietly(new File(qrPath));
	        FileUtils.deleteQuietly(new File(mainPath));
	        FileUtils.deleteQuietly(new File(newQrImagePath));
	        return newQrImageName;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
}
