package com.cms.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cms.entity.Commission;
import com.cms.entity.Member;
import com.cms.entity.Order;
import com.cms.entity.Payment;

public class CommissionUtils {

	public static void commission(Order order){
		//fx
		List<Commission> commissions = new Commission().dao().findByOrderId(order.getId());
		if(CollectionUtils.isNotEmpty(commissions)){
			for(Commission commission : commissions){
				commission.setModifyDate(new Date());
				commission.setStatus(2);
				commission.update();
				
				Payment payment = new Payment();
				payment.setMemo(commission.getTitle());
				payment.setType(Payment.Type.COMMISSION.ordinal());
				payment.setCreateDate(new Date());
				payment.setModifyDate(new Date());
				payment.setMemberId(commission.getMemberId());
				payment.setOrderId(order.getId());
				payment.setAmount(commission.getAmount());
				payment.setInout("+");
				payment.save();
				
				
				Member member = payment.getMember();
				BigDecimal newAmount = member.getAmount().add(commission.getAmount());
				member.setAmount(newAmount);
				member.update();
			}
		}
	}
}
