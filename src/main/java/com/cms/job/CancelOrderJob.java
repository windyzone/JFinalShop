package com.cms.job;

import java.util.Date;
import java.util.List;

import com.cms.entity.Order;

public class CancelOrderJob implements Runnable{

 	@Override
    public void run() {
 		List<Order> orders = new Order().dao().findList(Order.Status.PENDING_PAYMENT.ordinal(),null,null);
        for(Order order : orders){
           long current = new Date().getTime();
           long expire = order.getExpireDate().getTime();
           if(current>expire){
        	   order.setStatus(-1);
        	   order.setModifyDate(new Date());
        	   order.update();
           }
        }
 	}
}
