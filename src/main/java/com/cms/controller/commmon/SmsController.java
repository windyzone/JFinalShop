package com.cms.controller.commmon;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cms.Feedback;
import com.cms.entity.Member;
import com.cms.entity.MessageConfig;
import com.cms.entity.SafeKey;
import com.cms.routes.RouteMapping;
import com.cms.util.SmsUtils;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;

@RouteMapping(url = "/common/sms")
public class SmsController extends Controller{

    
    public void sendRegister(){
    	if(!validateCaptcha("captcha")){
    		renderJson(Feedback.error("验证码错误!"));
    		return;
    	}
    	String mobile = getPara("mobile");
    	 Member pMember = new Member().dao().findByMobile(mobile);
         if(pMember!=null){
         	renderJson(Feedback.error("用户已存在!"));
     		return;
         }
        send(MessageConfig.Type.REGISTER,mobile);
    }
    
    public void sendLogin(){
    	if(!validateCaptcha("captcha")){
    		renderJson(Feedback.error("验证码错误!"));
    		return;
    	}
    	String mobile = getPara("mobile");
    	 Member pMember = new Member().dao().findByMobile(mobile);
         if(pMember==null){
         	renderJson(Feedback.error("用户不存在!"));
     		return;
         }
        send(MessageConfig.Type.LOGIN,mobile);
    }
    
    public void sendAccount(){
    	if(!validateCaptcha("captcha")){
    		renderJson(Feedback.error("验证码错误!"));
    		return;
    	}
        Member currentMember = (Member) getSession().getAttribute(Member.SESSION_MEMBER);
        Member member = new Member().dao().findById(currentMember.getId());
        send(MessageConfig.Type.SET_ACCOUNT,member.getMobile());
    }
    
    public void sendForgetPassword(){
    	String mobile = getPara("mobile");
    	Member pMember = new Member().dao().findByMobile(mobile);
        if(pMember==null){
        	renderJson(Feedback.error("用户不存在!"));
    		return;
        }
        send(MessageConfig.Type.FORGET_PASSWORD,mobile);
    }
    
    public void sendUpdatePassword(){
    	String mobile = getPara("mobile");
        send(MessageConfig.Type.UPDATE_PASSWORD,mobile);
    }
    
    
    @NotAction
    public void send(MessageConfig.Type type,String mobile){
        String code = RandomStringUtils.randomNumeric(4);
        SafeKey safeKey = new SafeKey();
        safeKey.setExpire(DateUtils.addHours(new Date(), 1));
        safeKey.setMobile(mobile);
        safeKey.setCreateDate(new Date());
        safeKey.setModifyDate(new Date());
        safeKey.setValue(code);
        safeKey.setType(type.ordinal());
        safeKey.save();
        switch (type) {
            case REGISTER:
                SmsUtils.sendRegisterMemberSms(code,mobile);
                break;
            case FORGET_PASSWORD:
                SmsUtils.sendForgetPasswordSms(code,mobile);
                break;   
            case UPDATE_PASSWORD:
                SmsUtils.sendUpdatePasswordSms(code,mobile);
                break;
            case SET_ACCOUNT:
                SmsUtils.sendSetAccountSms(code,mobile);
                break;
            case LOGIN:
                SmsUtils.sendLoginSms(code,mobile);
                break;
        }
        Map<String,Object> data = new HashMap<>();
        data.put("code", code);
        renderJson(Feedback.success(data));
    }
    
}
