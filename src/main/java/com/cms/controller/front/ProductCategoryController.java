package com.cms.controller.front;

import com.cms.routes.RouteMapping;

@RouteMapping(url = "/product_category")
public class ProductCategoryController extends BaseController{
	
	/**
	 * 首页
	 */
	public void index(){
		Long productCategoryId = getParaToLong("productCategoryId");
		if(productCategoryId!=null){
			setAttr("productCategoryId", productCategoryId);
		}
	    render("/templates/"+getTheme()+"/"+getDevice()+"/product_category.html");
	}

}
