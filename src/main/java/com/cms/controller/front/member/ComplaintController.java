package com.cms.controller.front.member;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Complaint;
import com.cms.entity.FriendLink;
import com.cms.routes.RouteMapping;
import com.cms.util.SystemUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.HashMap;

@RouteMapping(url = "/member/complaint")
public class ComplaintController extends BaseController {


    /**
     * 添加
     */
    public void add() {
        render("/templates/"+getTheme()+"/"+getDevice()+"/member_complaint_add.html");
    }

    /**
     * 保存
     */
    public void save() {
        Complaint complaint = getModel(Complaint.class,"",true);
        if(StringUtils.isNotBlank(complaint.getMaterial())){
            String material = complaint.getMaterial();
            material = material.replace(SystemUtils.getConfig().getImgUrl(), "");
            complaint.setMaterial(material);
        }
        complaint.setCreateDate(new Date());
        complaint.setModifyDate(new Date());
        complaint.save();
        renderJson(Feedback.success(new HashMap<>()));
    }

}
