package com.cms.controller.front.member;

import java.util.List;

import com.cms.controller.front.BaseController;
import com.cms.entity.Order;
import com.cms.entity.Pack;
import com.cms.routes.RouteMapping;
import com.cms.util.fegine.FegineApi;
import com.cms.util.fegine.FegineResponse;
@RouteMapping(url = "/member/pack")
public class PackController extends BaseController{

	public void index(){
		Long orderId = getParaToLong("orderId");
		Order order = new Order().dao().findById(orderId);
		setAttr("order", order);
		List<Pack> packs = new Pack().dao().findByOrderId(orderId);
		setAttr("packs", packs);
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_pack.html");
	}
	
	public void traffic(){
		Long packId = getParaToLong("packId");
		Pack pack = new Pack().dao().findById(packId); 
		FegineResponse response = FegineApi.getInfo(pack.getExpressNumber(), Pack.ExpressCode.expressCodeValueMap.get(pack.getExpressCode()).name());
		setAttr("infos", response.getResult().getList());
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_traffic.html");
	}
}
