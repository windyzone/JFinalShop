package com.cms.controller.front.member;

import java.util.HashMap;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.entity.MessageConfig;
import com.cms.entity.SafeKey;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/bind")
public class BindController extends BaseController{

	public void index(){
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_bind.html");
	}
	
	public void save(){
		String username = getPara("username");
		String code = getPara("code");
		SafeKey safeKey = new SafeKey().dao().findByMobile(username, MessageConfig.Type.REGISTER.ordinal());
		if(!(safeKey.getValue().equals(code))){
			renderJson(Feedback.error("短信验证码错误"));
			return;
		}
		Member member = new Member().dao().findById(getCurrentMember().getId());
		member.setMobile(username);
		member.update();
		renderJson(Feedback.success(new HashMap<>()));
	}
}
