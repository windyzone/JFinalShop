package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.entity.Coupon;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/coupon")
public class CouponController extends BaseController{

	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		Boolean isValid = getParaToBoolean("isValid");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Coupon().dao().findMemberPage(currentMember.getId(),isValid,pageNumber,pageSize));
		setAttr("isValid", isValid);
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_coupon_list.html");
	}
}
