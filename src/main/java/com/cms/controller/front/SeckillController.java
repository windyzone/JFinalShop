package com.cms.controller.front;

import com.cms.entity.Seckill;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/seckill")
public class SeckillController extends BaseController{

	/**
	 * 详情页
	 */
	public void detail(){
		Long seckillId=getParaToLong(0);
		Seckill seckill = new Seckill().dao().findById(seckillId);
        setAttr("seckill", seckill);
		render("/templates/"+getTheme()+"/"+getDevice()+"/seckill_detail.html");
	}
}
