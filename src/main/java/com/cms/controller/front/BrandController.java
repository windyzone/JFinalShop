package com.cms.controller.front;

import com.cms.entity.Brand;
import com.cms.entity.Product;
import com.cms.routes.RouteMapping;

/**
 * Controller - 品牌
 * 
 * 
 * 
 */
@RouteMapping(url = "/brand")
public class BrandController extends BaseController{
	
    /**
     * 每页记录数
     */
    private static final int PAGE_SIZE = 20;
    

	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 20 ; 
		setAttr("page",new Brand().dao().findPage(null,pageNumber,pageSize));
		render("/templates/"+getTheme()+"/"+getDevice()+"/brand_list.html");
	}
	
	/**
	 * 列表
	 */
	public void detail(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 20 ; 
		Long brandId = getParaToLong(0);
		Brand brand = new Brand().dao().findById(brandId);
        setAttr("brand", brand);
        setAttr("page",new Product().dao().findPage(pageNumber,pageSize,null,brandId,true,null,null,null));
		render("/templates/"+getTheme()+"/"+getDevice()+"/brand_detail.html");
	}
}
