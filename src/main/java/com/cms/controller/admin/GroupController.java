package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.cms.Feedback;
import com.cms.entity.Group;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/group")
public class GroupController extends BaseController {

	/**
	 * 添加
	 */
	public void add() {
		render(getView("group/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		Group group = getModel(Group.class,"",true); 
		group.setCreateDate(new Date());
		group.setModifyDate(new Date());
		group.save();
		redirect(getListQuery("/admin/group/list"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("group", new Group().dao().findById(id));
		render(getView("group/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		Group group = getModel(Group.class,"",true); 
		group.setModifyDate(new Date());
		group.update();
		redirect(getListQuery("/admin/group/list"));
	}
	
	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new Group().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("group/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new Group().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
