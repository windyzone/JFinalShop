package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.Config;
import com.cms.Feedback;
import com.cms.entity.Brand;
import com.cms.routes.RouteMapping;
import com.cms.util.SystemUtils;
import com.jfinal.kit.PropKit;

@RouteMapping(url = "/admin/brand")
public class BrandController extends BaseController {

	/**
	 * 添加
	 */
	public void add() {
		render(getView("brand/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		Brand brand = getModel(Brand.class,"",true); 
		if(StringUtils.isNotBlank(brand.getImage())){
        	String image = brand.getImage();
        	image = image.replace(SystemUtils.getConfig().getImgUrl(), "");
        	brand.setImage(image);
        }
		brand.setCreateDate(new Date());
		brand.setModifyDate(new Date());
		brand.save();
		redirect(getListQuery("/admin/brand/list"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("brand", new Brand().dao().findById(id));
		render(getView("brand/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		Brand brand = getModel(Brand.class,"",true); 
		if(StringUtils.isNotBlank(brand.getImage())){
        	String image = brand.getImage();
        	image = image.replace(SystemUtils.getConfig().getImgUrl(), "");
        	brand.setImage(image);
        }
		brand.setModifyDate(new Date());
		brand.update();
		redirect(getListQuery("/admin/brand/list"));
	}
	
	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new Brand().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("brand/list"));
	}
	
	/**
     * 修改排序
     */
    public void updateSort(){
        String sortArray = getPara("sortArray");
        JSONArray jsonArray = JSONArray.parseArray(sortArray);
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long id = jsonObject.getLong("id");
            Integer sort = jsonObject.getInteger("sort");
            Brand brand = new Brand().dao().findById(id);
            brand.setSort(sort);
            brand.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new Brand().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
