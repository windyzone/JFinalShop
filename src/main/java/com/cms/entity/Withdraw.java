package com.cms.entity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cms.entity.base.BaseWithdraw;
import com.cms.util.DBUtils;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * 提现
 */
@SuppressWarnings("serial")
public class Withdraw extends BaseWithdraw<Withdraw> {
	
	
	/**
     * 佣金状态
     */
	public enum Status{
		PENDING_CHECK("待审核"),
		PENDING_PAYMENT("待打款"),
		COMPLETED("已完成"),
		REJECTED("已驳回");
		public String text;
		Status(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
		public static Map<Integer, Withdraw.Status> statusValueMap = new HashMap<>();
	    static {
            Withdraw.Status[] values = Withdraw.Status.values();
	        for (Withdraw.Status status : values) {
	        	statusValueMap.put(status.ordinal(), status);
	        }
	    }
	}
	
	public String getStatusName(){
		return Withdraw.Status.statusValueMap.get(getStatus()).getText();
	}
	
    /**
     * 会员
     */
    private Member member;
    
    
    /**
     * 获取会员
     * 
     * @return  会员
     */
    public Member getMember(){
        if(member == null){
            member = new Member().dao().findById(getMemberId());
        }
        return member;
    }
    
    /**
     * 查找提现金额
     * @return
     */
    public BigDecimal findAmount(Long memberId,Integer status){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        if(status!=null){
            filterSql+=" and status="+status;
        }
        return Db.queryBigDecimal("select sum(amount) from cms_withdraw where 1=1 "+filterSql);
    }
    
    /**
     * 查找提现分页
     * 
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @param memberId
     *            会员ID
     * @return 提现分页
     */
    public Page<Withdraw> findPage(Integer pageNumber,Integer pageSize,Long memberId,Integer status){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        if(status!=null){
            filterSql+=" and status="+status;
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_withdraw where 1=1 "+filterSql+orderBySql);
    }
    
    /**
     * 查找提现分页
     * 
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @return 订单分页
     */
    public Page<Withdraw> findPage(Status status,Integer pageNumber,Integer pageSize){
        String filterSql = "";
        if(status!=null){
            filterSql+=" and status="+status.ordinal();
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_withdraw where 1=1 "+filterSql+orderBySql);
    }
    
    
    /**
     * 查找提现
     * 
     * @return 提现
     */
    public List<Withdraw> findList(Long memberId,Integer status){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        if(status!=null){
            filterSql+=" and status="+status;
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return find("select * from cms_withdraw where 1=1 "+filterSql+orderBySql);
    }
}
