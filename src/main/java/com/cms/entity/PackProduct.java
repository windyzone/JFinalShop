package com.cms.entity;

import com.cms.entity.base.BasePackProduct;
import com.jfinal.plugin.activerecord.Db;

/**
 * 包裹商品
 */
@SuppressWarnings("serial")
public class PackProduct extends BasePackProduct<PackProduct> {
	
    /**
     * 商品
     */
    private Product product;
    
   /**
     * 获取商品
     * 
     * @return 商品
     */
    public Product getProduct(){
        if(product == null){
            product = new Product().dao().findById(getProductId());
        }
        return product;
    }
	
	public void deleteByPackId(Long packId){
		Db.delete("delete from cms_pack_product where packId=?",packId);
	}
	
}
